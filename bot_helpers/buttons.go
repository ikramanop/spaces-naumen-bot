package bot_helpers

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"spaces-naumen-bot/data"

	tb "gopkg.in/tucnak/telebot.v2"
)

var GetCurrentInternsButton = tb.InlineButton{
	Unique: "getCurrentInternsButton",
	Text:   "Список стажировок",
}

var StartQuestioningButton = tb.InlineButton{
	Unique: "startQuestioningButton",
	Text:   "Заполнить анкету",
}

var CloseDialogueButton = tb.InlineButton{
	Unique: "closeDialogueButton",
	Text:   "В начало",
}

var GetInternshipInfoButton = tb.InlineButton{
	Unique: "getInternshipInfoButton",
	Text:   "Информация о стажировке",
}

var GetEducationInfoButton = tb.InlineButton{
	Unique: "getEducationInfoButton",
	Text:   "Образовательные программы NAUMEN",
}

var GetNotificationsSettingsButton = tb.InlineButton{
	Unique: "getNotificationsSettings",
	Text:   "Настройка уведомлений",
}

var EnableNotificationsButton = tb.InlineButton{
	Unique: "enableNotificationsButton",
	Text:   "Включить",
}

var DisableNotificationsButton = tb.InlineButton{
	Unique: "disableNotificationsButton",
	Text:   "Выключить",
}

var IHaveQuestionsButton = tb.InlineButton{
	Unique: "iHaveQuestionsButton",
	Text:   "Есть вопросы?",
}

func GetCitiesButtons(host string, httpClient *http.Client, availableInterns map[string]data.InternData) (*[][]tb.InlineButton, error) {
	response, err := httpClient.Get(fmt.Sprintf("http://%s:5000/get/interns", host))
	if err != nil || response.StatusCode != 200 {
		return nil, err
	}
	defer func() { _ = response.Body.Close() }()

	err = json.NewDecoder(response.Body).Decode(&availableInterns)
	if err != nil {
		return nil, err
	}

	citiesButtons := make([][]tb.InlineButton, 0, len(availableInterns)+1)
	for city, interns := range availableInterns {
		citiesButtons = append(citiesButtons, []tb.InlineButton{
			{
				Unique: city,
				Text:   fmt.Sprintf("%s (кол-во: %d)", city, len(interns.Interns)),
			},
		})
	}
	citiesButtons = append(citiesButtons, []tb.InlineButton{CloseDialogueButton})

	return &citiesButtons, nil
}

func GetNotificationStatus(host string, httpClient *http.Client, chatId int) (bool, error) {
	response, err := httpClient.Get(fmt.Sprintf("http://%s:5000/get/wait/%d", host, chatId))
	if err != nil {
		return false, err
	} else if response.StatusCode == 200 {
		return true, nil
	} else if response.StatusCode == 400 {
		return false, nil
	} else {
		return false, errors.New("error getting notification")
	}
}

func EnableNotification(host string, httpClient *http.Client, chatId int) (bool, error) {
	response, err := httpClient.PostForm(fmt.Sprintf("http://%s:5000/set/wait/%d", host, chatId), url.Values{})
	if err != nil {
		return false, err
	} else if response.StatusCode == 200 {
		return true, nil
	} else if response.StatusCode == 400 {
		return false, nil
	} else {
		return false, errors.New("error changing notification")
	}
}

func DisableNotification(host string, httpClient *http.Client, chatId int) (bool, error) {
	response, err := httpClient.PostForm(fmt.Sprintf("http://%s:5000/delete/wait/%d", host, chatId), url.Values{})
	if err != nil {
		return false, err
	}
	if response.StatusCode == 200 {
		return true, nil
	} else if response.StatusCode == 200 {
		return true, nil
	} else if response.StatusCode == 400 {
		return false, nil
	} else {
		return false, errors.New("error changing notification")
	}
}
