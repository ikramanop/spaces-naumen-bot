module spaces-naumen-bot

go 1.15

require (
	github.com/go-redis/redis/v8 v8.8.2
	gopkg.in/tucnak/telebot.v2 v2.3.5
)
