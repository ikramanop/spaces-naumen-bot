export BOT_NAME=spaces-naumen-bot
export CURRENT_BRANCH = $(shell git symbolic-ref --short HEAD)

build:
	go build -o bin/bot *.go

build-docker:
	@docker build \
		-t ${BOT_NAME}:${CURRENT_BRANCH} \
		.

run:
	@make build
	@./bin/bot

run-debug:
	go run *.go

run-docker:
	docker run -d ${BOT_NAME}:${CURRENT_BRANCH}

run-all:
	docker-compose up -d

down-all:
	docker-compose down