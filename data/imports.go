package data

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

func ImportJson(path string) (*map[string]string, error) {
	jsonFile, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	jsonData, err := ioutil.ReadAll(jsonFile)
	if err != nil {
		return nil, err
	}

	var questions map[string]string

	err = json.Unmarshal(jsonData, &questions)
	if err != nil {
		return nil, err
	}

	err = jsonFile.Close()
	if err != nil {
		return nil, err
	}

	return &questions, nil
}
