package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"spaces-naumen-bot/bot_helpers"
	"spaces-naumen-bot/data"

	"github.com/go-redis/redis/v8"
	tb "gopkg.in/tucnak/telebot.v2"
)

func main() {
	questions, err := data.ImportJson("static/questions.json")
	if err != nil {
		log.Fatalln(err)
	}

	var availableInterns = map[string]data.InternData{}

	apiHost := os.Getenv("API_HOST")
	if apiHost == "" {
		apiHost = "localhost"
	}
	redisHost := os.Getenv("REDIS_HOST")
	if redisHost == "" {
		redisHost = "localhost"
	}
	tgBotToken := os.Getenv("TG_BOT_TOKEN")
	if redisHost == "" {
		log.Fatalln("Can't start bot without a token provided")
	}
	rs, err := bot_helpers.NewRedis(fmt.Sprintf("%s:6379", redisHost))
	if err != nil {
		log.Fatalln(err)
	}

	b, err := tb.NewBot(tb.Settings{
		Token:  tgBotToken,
		Poller: &tb.LongPoller{Timeout: 10 * time.Second},
	})
	if err != nil {
		log.Fatal(err)
	}

	httpClient := &http.Client{Timeout: 10 * time.Second}

	b.Handle("/start", func(m *tb.Message) {
		_, err := rs.Get(m.Sender.Username)
		if err == redis.Nil {
			_, _ = b.Send(m.Sender, (*questions)["welcomeMessage"], &tb.ReplyMarkup{InlineKeyboard: bot_helpers.MainMenuKeys})
			rs.Set(m.Sender.Username, bot_helpers.Start)
			return
		} else if err != nil {
			_, _ = b.Send(m.Sender, "Произошла ошибка")
			return
		}
	})

	b.Handle("/stop", func(m *tb.Message) {
		_, err := rs.Get(m.Sender.Username)
		if err != nil {
			return
		}

		rs.Delete(m.Sender.Username)
		_, _ = b.Send(m.Sender, "Ждём Вас снова.")
	})

	b.Handle(&bot_helpers.CloseDialogueButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		rs.Set(c.Sender.Username, bot_helpers.Start)
		_ = b.Delete(c.Message)
		_, _ = b.Send(c.Sender, "Выберите опцию!", &tb.ReplyMarkup{InlineKeyboard: bot_helpers.MainMenuKeys})
	})

	b.Handle(&bot_helpers.GetInternshipInfoButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		_ = b.Delete(c.Message)
		_, _ = b.Send(c.Sender, (*questions)["internshipInfo"], &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{InlineKeyboard: bot_helpers.CloseKeys},
			ParseMode:   "Markdown",
		})
		rs.Set(c.Sender.Username, bot_helpers.WaitingForClose)
	})

	b.Handle(&bot_helpers.GetEducationInfoButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		_ = b.Delete(c.Message)
		_, _ = b.Send(c.Sender, (*questions)["education"], &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{InlineKeyboard: bot_helpers.CloseKeys},
			ParseMode:   "Markdown",
		})
		rs.Set(c.Sender.Username, bot_helpers.WaitingForClose)
	})

	b.Handle(&bot_helpers.StartQuestioningButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		_ = b.Delete(c.Message)
		_, _ = b.Send(c.Sender, (*questions)["questioning"], &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{InlineKeyboard: bot_helpers.CloseKeys},
			ParseMode:   "Markdown",
		})
		rs.Set(c.Sender.Username, bot_helpers.WaitingForClose)
	})

	b.Handle(&bot_helpers.GetNotificationsSettingsButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		_ = b.Delete(c.Message)
		status, err := bot_helpers.GetNotificationStatus(apiHost, httpClient, c.Sender.ID)
		if status {
			_, _ = b.Send(c.Sender, "Выключить уведомления о новых стажировках?",
				&tb.ReplyMarkup{InlineKeyboard: bot_helpers.DisableNotificationsKeys})
		} else if err == nil {
			_, _ = b.Send(c.Sender, "Включить уведомления о новых стажировках?",
				&tb.ReplyMarkup{InlineKeyboard: bot_helpers.EnableNotificationsKeys})
		} else {
			_, _ = b.Send(c.Sender, "Не удаётся получить настройки уведомлений")
			_, _ = b.Send(c.Sender, "Выберите опцию!", &tb.ReplyMarkup{InlineKeyboard: bot_helpers.MainMenuKeys})
		}
	})

	b.Handle(&bot_helpers.EnableNotificationsButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		_ = b.Delete(c.Message)
		status, err := bot_helpers.EnableNotification(apiHost, httpClient, c.Sender.ID)
		if err != nil || !status {
			log.Println(err)
			_, _ = b.Send(c.Sender, "Произошла ошибка при изменении настроек :(")
		} else {
			_, _ = b.Send(c.Sender, "Настройки уведомлений изменены")
		}

		rs.Set(c.Sender.Username, bot_helpers.Start)
		_, _ = b.Send(c.Sender, "Выберите опцию!", &tb.ReplyMarkup{InlineKeyboard: bot_helpers.MainMenuKeys})
	})

	b.Handle(&bot_helpers.DisableNotificationsButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		_ = b.Delete(c.Message)
		status, err := bot_helpers.DisableNotification(apiHost, httpClient, c.Sender.ID)
		if err != nil || !status {
			log.Println(err)
			_, _ = b.Send(c.Sender, "Произошла ошибка при изменении настроек :(")
		} else {
			_, _ = b.Send(c.Sender, "Настройки уведомлений изменены")
		}

		rs.Set(c.Sender.Username, bot_helpers.Start)
		_, _ = b.Send(c.Sender, "Выберите опцию!", &tb.ReplyMarkup{InlineKeyboard: bot_helpers.MainMenuKeys})
	})

	b.Handle(&bot_helpers.GetCurrentInternsButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		_ = b.Delete(c.Message)

		_ = b.Respond(c, &tb.CallbackResponse{
			ShowAlert: false,
		})

		citiesButtons, err := bot_helpers.GetCitiesButtons(apiHost, httpClient, availableInterns)
		if err != nil {
			log.Println(err)
			_, _ = b.Send(c.Sender, "Произошла ошибка при получении списка стажировок :(")
			rs.Set(c.Sender.Username, bot_helpers.Start)
			_, _ = b.Send(c.Sender, "Выберите опцию!", &tb.ReplyMarkup{InlineKeyboard: bot_helpers.MainMenuKeys})
			return
		}

		_, _ = b.Send(c.Sender, "Выберите Ваш город: ", &tb.ReplyMarkup{InlineKeyboard: *citiesButtons})

		rs.Set(c.Sender.Username, bot_helpers.ChoosingCity)
	})

	b.Handle(tb.OnCallback, func(c *tb.Callback) {
		state, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		if state == bot_helpers.ChoosingCity {
			for city, interns := range availableInterns {
				_ = b.Delete(c.Message)
				if strings.Contains(c.Data, city) {
					sendData := ""

					if len(interns.Interns) == 0 {
						sendData = "К сожалению, в этом городе пока что нет стажировок :("
					} else {
						sendData = "В выбранном городе возможны следующие стажировки:\n\n"
						for _, intern := range interns.Interns {
							sendData += fmt.Sprintf(
								"%s\n[Описание](%s)\n[Тестовое задание](%s)\n\n",
								intern[0], intern[1], intern[2],
							)
						}
						sendData += "\nПодробно о стажировках можно прочитать по ссылкам!"
					}

					_, _ = b.Send(c.Sender, sendData, &tb.SendOptions{
						ReplyMarkup: &tb.ReplyMarkup{InlineKeyboard: bot_helpers.CloseForCitiesKeys},
						ParseMode:   "Markdown",
					})
				}
			}
		} else {
			_, _ = b.Send(c.Sender, "Пожалуйста, выберите одну из опций")
		}
	})

	b.Handle(&bot_helpers.IHaveQuestionsButton, func(c *tb.Callback) {
		_, err := rs.Get(c.Sender.Username)
		if err != nil {
			return
		}

		_ = b.Delete(c.Message)
		_, _ = b.Send(c.Sender, (*questions)["help"], &tb.SendOptions{
			ReplyMarkup: &tb.ReplyMarkup{InlineKeyboard: bot_helpers.CloseKeys},
			ParseMode:   "Markdown",
		})
		rs.Set(c.Sender.Username, bot_helpers.WaitingForClose)
	})

	b.Start()
}
