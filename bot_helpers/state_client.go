package bot_helpers

import (
	"context"

	"github.com/go-redis/redis/v8"
)

type Redis struct {
	Client *redis.Client
	ctx    context.Context
}

func NewRedis(addr string) (*Redis, error) {
	rs := &Redis{
		Client: redis.NewClient(&redis.Options{
			Addr:     addr,
			Password: "",
			DB:       0,
		}),
		ctx: context.Background(),
	}

	_, err := rs.Client.Ping(rs.ctx).Result()
	if err != nil {
		return nil, err
	}

	rs.clearAll()

	return rs, nil
}

func (rs Redis) clearAll() {
	rs.Client.Do(rs.ctx, "FLUSHALL")
}

func (rs Redis) Set(key, value string) bool {
	err := rs.Client.Set(rs.ctx, key, value, 0).Err()
	if err != nil {
		return false
	}

	return true
}

func (rs Redis) Get(key string) (string, error) {
	value, err := rs.Client.Get(rs.ctx, key).Result()
	if err != nil {
		return "", err
	}

	return value, nil
}

func (rs Redis) Delete(key string) {
	rs.Client.Del(rs.ctx, key)
}
