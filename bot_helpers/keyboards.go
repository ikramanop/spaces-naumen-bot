package bot_helpers

import tb "gopkg.in/tucnak/telebot.v2"

var MainMenuKeys = [][]tb.InlineButton{
	{GetInternshipInfoButton},
	{GetEducationInfoButton},
	{GetCurrentInternsButton},
	{StartQuestioningButton},
	{GetNotificationsSettingsButton},
	{IHaveQuestionsButton},
}

var CloseKeys = [][]tb.InlineButton{
	{CloseDialogueButton},
}

var CloseForCitiesKeys = [][]tb.InlineButton{
	{GetCurrentInternsButton},
	{CloseDialogueButton},
}

var EnableNotificationsKeys = [][]tb.InlineButton{
	{EnableNotificationsButton},
	{CloseDialogueButton},
}

var DisableNotificationsKeys = [][]tb.InlineButton{
	{DisableNotificationsButton},
	{CloseDialogueButton},
}
