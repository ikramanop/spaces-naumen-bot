# Spaces

## :robot: [CASE "NAUMEN Telegram Бот"](https://drive.google.com/file/d/1EyuxQHTg5V7LGInKZFtsyVkFNT3FBaEz/view)

> Наше решение позволяет HR-ам быстро и удобно обрабатывать анкеты будущих стажеров. Вся информация собирается в одном месте, [Гугл-Таблице](https://docs.google.com/spreadsheets/d/1OMjENvfDnax3xV9saBWAbM111KxTskO-CBaNUTYfQTk/edit?usp=sharing), где ее очень просто анализировать. Помимо удобства для HR-ов, [Telegram Бот](https://t.me/spaces_naumen_bot) будет полезен и будущим стажерам, показывая актуальную информацию о доступных стажировках и образовательных программах. Также бот может оповещать стажеров об открывшихся стажировках в их городе и давать возможность заполнить анкету в [Гугл-Форме](https://forms.gle/8RoffafEfF9fW1wPA).

## Наш стек для бота

- Golang
- Telegram API
- Redis
- Docker

## Как это работает?

1. Студент общается с ботом и получает ссылку на Гугл-Форму для прохождения анкеты.
2. Студент отправляет анкету с тестовым заданием.
3. Форма отправляет запрос к серверу, который считывает таблицу с ответами, и форматирует ее в более читабельный вид.
4. HR видит изменения в таблице.

## Docker

Бот докеризирован и может быть собран с помощью команды `make build-docker`

После сборки бота вместе с Redis можно запустить с помощью команды `make run-all`

## Замечание

Для запуска необходимо указать токен для telegram-бота в переменную окружения TG_BOT_TOKEN.
При запуске через docker-compose указать токен в разделе environments (файл `docker-compose.yml`)