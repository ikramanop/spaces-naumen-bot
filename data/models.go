package data

type InternData struct {
	Count   int        `json:"count"`
	Interns [][]string `json:"interns"`
}
