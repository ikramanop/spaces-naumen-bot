FROM golang:alpine as builder

RUN apk add --update --no-cache make git

WORKDIR /build_dir

COPY . /build_dir

RUN make build


FROM alpine:latest as runner

COPY --from=builder /build_dir/bin .

ENTRYPOINT ["./bot"]
